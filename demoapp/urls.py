from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from demoapp import views

urlpatterns = [
  url(r'^$', views.HomePageView.as_view()),
]